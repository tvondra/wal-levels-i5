PATH=/home/postgres/pg-master/bin:$PATH

while true; do

        psql -t -A -c "select extract(epoch from now()) AS ts, wait_event_type, wait_event, count(*) from pg_stat_activity group by 1, 2, 3" postgres >> wait-events.log 2> /dev/null

        psql -c "select pg_sleep(0.1)" postgres > /dev/null 2>&1;

done;

