PATH=/home/postgres/pg-9.6/bin:$PATH

while true; do

        psql -t -A -c "select now(), extract(epoch from now()), pg_current_xlog_location(), pg_xlog_location_diff(pg_current_xlog_location(), '0/0');" postgres >> xlog.log  2> /dev/null

        psql -t -A -c "select now(), extract(epoch from now()), * from pg_stat_user_tables" pgbench >> stat-user-tables.log 2> /dev/null
        psql -t -A -c "select now(), extract(epoch from now()), * from pg_statio_user_tables" pgbench >> statio-user-tables.log 2> /dev/null

        psql -t -A -c "select now(), extract(epoch from now()), * from pg_stat_user_indexes" pgbench >> stat-user-indexes.log 2> /dev/null
        psql -t -A -c "select now(), extract(epoch from now()), * from pg_statio_user_indexes" pgbench >> statio-user-indexes.log 2> /dev/null

        psql -t -A -c "select now(), extract(epoch from now()), relname, pg_relation_size(relid) from pg_stat_user_tables" pgbench >> table-sizes.log 2> /dev/null
        psql -t -A -c "select now(), extract(epoch from now()), indexrelname, pg_relation_size(indexrelid) from pg_stat_user_indexes" pgbench >> index-sizes.log 2> /dev/null

        psql -t -A -c "select now(), extract(epoch from now()), * from pg_stat_bgwriter" pgbench >> stat-bgwriter.log 2> /dev/null

        psql -t -A -c "select now(), extract(epoch from now()), pg_database_size(datname), * from pg_stat_database" postgres >> databases.log 2> /dev/null

        sleep 1;

done;

