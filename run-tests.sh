#!/bin/sh

PATH=/home/postgres/pg-master/bin:$PATH
WARMUP=900
DURATION=$((3600))
SCALE=$1
RATE=$2
DIR=`pwd`
DATADIR=/mnt/raid/pgdata
WALDIR=/mnt/data/wal

./collect-stats.sh &
./collect-wait-events.sh &

for t in minimal replica logical; do

	name="$t-$SCALE"

	pg_ctl -D $DATADIR -w -t 3600 stop

	mkdir $name;

	cp postgresql-minimal.conf $DATADIR/postgresql.conf

	pg_ctl -D $DATADIR -l $name/pg.log -w start

	sleep 5

	dropdb --if-exists pgbench
	createdb pgbench

	pgbench -i -s $SCALE pgbench > pgbench.txt 2>&1

        cp postgresql-$t.conf $DATADIR/postgresql.conf

        pg_ctl -D $DATADIR -l $name/pg.log -w restart

        sleep 5

	psql -c "select * from pg_settings" postgres > $name/setting.log

	pgbench -c 32 -T $WARMUP pgbench > $name/warmup.log 2>&1

	psql -c "checkpoint" postgres

	rm -Rf $WALDIR/*
	rm *.log

	sleep 1

	pgbench -c 32 -T $DURATION -l --aggregate-interval=1 pgbench > $name/pgbench.log 2>&1

	mv pgbench_log.* $name
	mv *.log $name

	cd $WALDIR

	ls -l > $DIR/$name/xlog.list 2>&1

        s=`ls | head -n 1`
        e=`ls | tail -n 1`

        echo xlog $s $e

        pg_xlogdump --stats $s $e > $DIR/$name/xlogdump.stats.log 2>&1
        pg_xlogdump --stats=record $s $e > $DIR/$name/xlogdump.records.log 2>&1

	cd $DIR

done

for t in minimal replica logical; do

	name="$t-$SCALE-throttled"

        pg_ctl -D $DATADIR -w -t 3600 stop

        mkdir $name;

        cp postgresql-minimal.conf $DATADIR/postgresql.conf

        pg_ctl -D $DATADIR -l $name/pg.log -w start

        sleep 5

        dropdb --if-exists pgbench
        createdb pgbench

        pgbench -i -s $SCALE pgbench > pgbench.txt 2>&1

        cp postgresql-$t.conf $DATADIR/postgresql.conf

        pg_ctl -D $DATADIR -l $name/pg.log -w restart

        sleep 5

        psql -c "select * from pg_settings" postgres > $name/setting.log

	pgbench -c 32 -T $WARMUP pgbench > $name/warmup.log 2>&1

        psql -c "checkpoint" postgres

	rm -Rf $WALDIR/*
	rm *.log

	sleep 1

        pgbench -c 32 -T $DURATION -l --aggregate-interval=1 -R $RATE pgbench > $name/pgbench.log 2>&1

        mv pgbench_log.* $name
        mv *.log $name

        cd $WALDIR

        ls -l > $DIR/$name/xlog.list 2>&1

	s=`ls | head -n 1`
	e=`ls | tail -n 1`

	echo xlog $s $e

        pg_xlogdump --stats $s $e > $DIR/$name/xlogdump.stats.log 2>&1
        pg_xlogdump --stats=record $s $e > $DIR/$name/xlogdump.records.log 2>&1

        cd $DIR

done

kill `jobs -p`
